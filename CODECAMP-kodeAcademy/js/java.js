// function saludo(){
//     console.log("hola");
// }

// function suma(numero1, numero2){
//     let result=numero1+numero2;
//     // console.log("el resultado de la suma es:");
//     // console.log(result);
//     return result
// }

// let arreglo1 =[1,2,3];
// arreglo1.forEach(
//     (valor, indice, arreglo)=>{
//         console.log(`valor:${valor},Indice:${indice},Array:${arreglo}`);    
//     }
// );

function addNewCard(title, description, imgUrl,dataBaseId){
    let mainContainer =document.getElementById("main-container");
    let card=document.createElement("div");
    let image=document.createElement("img");
    let cardBody =document.createElement("div");
    let cardLink=document.createElement("a");
    let cardTitle=document.createElement("h4");
    let cardText=document.createElement("p");

    card.classList.add("card","product-card","mx-2");
    image.classList.add("card-img-top");
    image.src=imgUrl;
    cardTitle.classList.add("card-title");
    cardTitle.appendChild(cardLink);
    cardLink.href=`./product_details.html?id=${dataBaseId}`;
    cardLink.innerText=title;
    cardText.classList.add("card-text");
    cardText.innerText=description;
    cardBody.classList.add("card-body");

    cardBody.appendChild(cardTitle);
    cardBody.appendChild(cardText);
    card.appendChild(image);
    card.appendChild(cardBody);
    mainContainer.appendChild(card);
}

// function addNewRow(title, description, imgUrl, dataBaseId){
//     let mainContainer =document.getElementById("main-container");
//     let cardCol =document.createElement("div");
//     let card=document.createElement("div");
//     let imagen=document.createElement("img");
//     let cardBody=document.createElement("dev");
//     let cardTitle =document.createElement("h4");
//     let cardText=document.createElement("p");
//     let cardLink=document.createElement("a");

//     cardCol.appendChild(card);
//     card.appendChild(cardBody);
//     card.appendChild(imagen);
//     cardBody.appendChild(cardText);
//     cardBody.appendChild(cardTitle);
//     cardTitle.appendChild(cardLink);

// }


let database=firebase.database();
async function retrieveAllProducts() {
      let products={};
      await database
        .ref()
        .child("products")
        .get()
        .then((result)=>{
            if(result.exists()) {
                products=result.val();
            }else {
                console.log("items not found");
            }
        });
      return products;
}


 async function showAllProducts(){
    let allProducts=await retrieveAllProducts();
    listOfIds=Object.keys(allProducts);
    listOfIds.forEach(id => {
        let product=allProducts[id]
        addNewCard(product.name,product.description, product.imagenUrl,id);
    });
}

showAllProducts();




