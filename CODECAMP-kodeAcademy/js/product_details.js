let database =firebase.database();

const params=new URLSearchParams(window.location.search);

const productId= params.get("id");

 async function retrieveProduct(id){
     let product={};
     await database.ref().child("products").child(id).get().then((result)=>{
         if (result.exists()){
            product=result.val();
         }else{
             console.log(`Product ${id} not found`)
         }
     });
     return product;
 }

  async function showProductDetails(){
    let productTitle=document.getElementById("product_title");
    let productImagen=document.getElementById("product_imagen");
    let productDescription=document.getElementById("product_description");
    let product =await retrieveProduct(productId);
    productTitle.innerText=product.name;
    productImagen.setAttribute("src",product.imagenUrl);
    productDescription.innerText=product.description;
 }

 let deleteBtn =document.getElementById("btn-delete")
 deleteBtn.addEventListener("click",()=>{
     database.ref().child("products").child(productId).remove();
     window.location.href="/";
 });
 showProductDetails();

//  console.log( await retrieveProduct(productId));