let database=firebase.database();

 async function addProduct(name, description, imageUrl){
     return await database.ref().child("products").push({name:name,description:description,imagenUrl:imageUrl}).key;
};

function getProductData(){
    let product_name=document.getElementById("product_name_input").value;
    let product_description=document.getElementById("product_description_textarea").value;
    let imagenUrl=document.getElementById("imagen_url_input").value;

    let result={
        name:product_name,
        description:product_description,
        imagenUrl:imagenUrl
    }
    return result
};

let saveButton=document.getElementById("save-btn");
saveButton.addEventListener("click", async()=>{
    let result=getProductData();
    let response=  await addProduct(result.name, result.description, result.imagenUrl);
    alert(response);

} );